package net.zerodind.uhccore.nms;

import org.bukkit.Bukkit;

/**
 * Factory class used to create {@link NmsAdapter}s.
 */
public final class NmsAdapterFactory {

	/**
	 * Creates an {@link NmsAdapter} for the running server version.
	 *
	 * @return the adapter
	 * @throws CreateNmsAdapterException if the adapter can't be created
	 */
	public static NmsAdapter create() throws CreateNmsAdapterException {
		final String nmsVersion = getNmsVersion();
		if (nmsVersion == null) {
			throw new CreateNmsAdapterException("Unable to find CraftBukkit version");
		}
		try {
			Class<?> adapterImplClass = loadAdapterImplClass(nmsVersion);
			return (NmsAdapter) adapterImplClass.getConstructor().newInstance();
		} catch (ReflectiveOperationException | LinkageError | SecurityException
				| IllegalArgumentException | ClassCastException e) {
			throw new CreateNmsAdapterException(
				"Unable to create adapter for NMS version " + nmsVersion, e);
		}
	}

	/**
	 * Loads and returns the adapter implementation class for a given NMS version.
	 *
	 * @param nmsVersion the NMS version string
	 * @return the adapter implementation class
	 * @throws ClassNotFoundException if the class cannot be located
	 * @throws LinkageError if the class cannot be linked
	 */
	private static Class<?> loadAdapterImplClass(String nmsVersion) throws ClassNotFoundException, LinkageError {
		final String adapterPackageName = NmsAdapter.class.getPackage().getName();
		final String adapterImplName = NmsAdapter.class.getSimpleName() + "Impl";
		return Class.forName(String.join(".", adapterPackageName, nmsVersion, adapterImplName));
	}

	/**
	 * Gets the NMS version of the running server, such as {@code v1_18_R2}.
	 *
	 * @return the NMS version, or {@code null} if not found
	 */
	private static String getNmsVersion() {
		// On Spigot, and on Paper for Minecraft <= 1.20.4, the server implementation class is located in a package named after the NMS version.
		// On Paper for Minecraft 1.20.5+, we can get the NMS version from MappingEnvironment#LEGACY_CB_VERSION.
		final String qualifiedNmsPackageName = Bukkit.getServer().getClass().getPackage().getName();
		final String nmsPackageName = qualifiedNmsPackageName.substring(qualifiedNmsPackageName.lastIndexOf('.') + 1);
		if (nmsPackageName.startsWith("v") && nmsPackageName.contains("_R")) {
			return nmsPackageName;
		} else {
			try {
				Class<?> mappingEnvClass = Class.forName("io.papermc.paper.util.MappingEnvironment");
				String cbVersion = (String) mappingEnvClass.getField("LEGACY_CB_VERSION").get(null);
				return cbVersion;
			} catch (ReflectiveOperationException e) {
				return null;
			} catch (ClassCastException e) {
				return null;
			}
		}
	}

}
